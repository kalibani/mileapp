import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

// the translations
// (tip move them in a JSON file and import them)
const resources = {
  en: {
    translation: {
      Kembali: 'Back to Mile',
      Hubungi: 'Call Us Now: +62 812-1133-5608',
      descriptionTop: 'Your one stop platform to manage all of your',
      descriptionBottom: 'field service management',
      placeholder: 'Enter your organization\'s name',
      BelumTerdaftar: 'Not registered yet?',
      contactUs: 'Contact us',
      forMoreInfo: 'for more info',
      errorRequired: 'The organization field is required'
    }
  },
  id: {
    translation: {
      Kembali: 'Kembali ke Mile',
      Hubungi: 'Hubungi Kami: +62 812-1133-5608',
      descriptionTop: 'Platform Anda untuk mengelola semua',
      descriptionBottom: 'manajemen layanan di lapangan',
      placeholder: 'Masukkan nama organisasi Anda',
      BelumTerdaftar: 'Belum terdaftar?',
      contactUs: 'Hubungi kami',
      forMoreInfo: 'untuk info lebih lanjut',
      errorRequired: 'Nama organisasi tidak boleh kosong'
    }
  }
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: 'id',

    keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

export default i18n;
