import {
  Switch, BrowserRouter as Router
} from 'react-router-dom';
import BaseRoute from 'config/BaseRoute';
import LoginLayout from 'Layout/LoginLayout';
import Login from 'container/Login';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <BaseRoute
            exact
            path="/"
            layout={LoginLayout}
            component={Login}
          />
          <BaseRoute
            exact
            path="/login"
            component={Login}
            layout={LoginLayout}
          />
        </Switch>

      </Router>
    </div>
  );
}

export default App;
