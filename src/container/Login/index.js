import { useState } from 'react';
import { Link } from 'react-router-dom';
import BaseInput from 'components/BaseInput';
import BaseButton from 'components/BaseButton';
import { useTranslation } from 'react-i18next';
import 'styles/login.scss';

const Login = () => {
  const [organizationName, setOrganizationName] = useState('');
  const [error, setError] = useState('');
  const [isDisabled, setIsDidabled] = useState(false);
  const { t } = useTranslation();

  const handleChange = (e) => {
    setError('');
    setOrganizationName(e.target.value);
  };
  const handleClick = () => {
    if (organizationName) {
      setIsDidabled(true);
      setTimeout(() => {
        setError(`oops :( ${organizationName} mile app is not available`);
        setIsDidabled(false);
      }, 1500);
    } else {
      const errorReq = t('errorRequired');
      setError(errorReq);
    }
  };
  return (
    <div className="container-login">
      <div className="wrapper-login">
        <div className="wrapper-image">
          <img src="https://taskdev.mile.app/69ece7b1819e760b63623e810922b59e.png" alt="Mile-App" />
        </div>
        <p className="description-login">
          {t('descriptionTop')}
          {' '}
          <br />
          {t('descriptionBottom')}
        </p>

        <div className="wrapper-action-login">
          <div className="wrapper-input">
            <BaseInput
              id="organizationName"
              name="organizationName"
              type="text"
              label={t('placeholder')}
              value={organizationName}
              onChange={handleChange}
              error={error}
            />
          </div>
          <div className="wrapper-button">
            <BaseButton
              onClick={handleClick}
              id="btn-login"
              variant="primary"
              disabled={isDisabled}
              full="true"
            >
              Login
            </BaseButton>
          </div>
        </div>

        <p className="description-login">
          {t('BelumTerdaftar')}
          {' '}
          <Link to="/register">{t('contactUs')}</Link>
          {' '}
          {t('forMoreInfo')}
        </p>
      </div>
    </div>
  );
};

Login.propTypes = {

};

export default Login;
