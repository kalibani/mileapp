import PropTypes from 'prop-types';
import BaseHeader from 'components/BaseHeader';
import BaseFooter from 'components/BaseFooter';
import './loginLayout.scss';

export default function LoginLayout({ children }) {
  return (
    <main>
      <BaseHeader />
      <div className="container container-main">
        {children}
      </div>
      <BaseFooter />
    </main>
  );
}

LoginLayout.defaultProps = {
  children: ''
};

LoginLayout.propTypes = {
  children: PropTypes.node
};
