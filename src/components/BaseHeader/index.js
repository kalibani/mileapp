/* eslint-disable jsx-a11y/label-has-associated-control */

import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import './header.scss';

const Header = () => {
  const { t, i18n } = useTranslation();
  const handleChangeLanguage = (e) => {
    const value = e.target.value;
    i18n.changeLanguage(value);
  };
  return (

    <div className="header">
      <div className="wrapper-back-button">
        <Link to="/mile">
          <span> &#8592; </span>
          {t('Kembali')}
        </Link>

      </div>
      <div className="wrapper-header">
        <label htmlFor="lamguage">
          {t('Hubungi')}
        </label>
        <select name="lamguage" id="lamguage" className="language" onChange={handleChangeLanguage}>
          <option value="id">Indonesiaaa</option>
          <option value="en">English</option>
        </select>
      </div>
    </div>
  );
};

// Header.defaultProps = {
//   t: () => {}
// };

// Header.propTypes = {
//   t: PropTypes.func
// };

export default Header;
