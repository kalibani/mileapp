import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import './input.scss';

const BaseInput = ({
  id, label, error, autoComplete, value, input, disabled, onInput, type, onChange
}) => {
  const [isActive, setIsActive] = useState(false);
  const [isFocused, setIsFocused] = useState(false);

  useEffect(() => {
    setIsActive(value && value);
    document.getElementById('organizationName').focus();
  }, [value]);

  const onFocus = () => {
    setIsActive(true);
    setIsFocused(true);
  };

  const onBlur = (e, callback) => {
    setIsFocused(false);

    if (e) {
      if (!value) {
        setIsActive(false);
      } else {
        setIsActive(true);
      }
    } else {
      setIsActive(false);
    }

    if (callback !== undefined) {
      callback();
    }
  };

  return (
    <div className="form-group position-relative custom">
      <label
        className={`form-group-label${(isActive ? ' active' : '')
          + (isFocused ? ' focused' : '')}`}
        htmlFor={id}
      >
        {label}
      </label>
      <input
        id={id}
        className={`form-control${isActive ? ' active' : ''}`}
        type={type}
        onInput={onInput}
        onChange={onChange}
        onFocus={onFocus}
        onBlur={(e) => onBlur(e, () => {
          onBlur(value);
        })}
        autoComplete={autoComplete}
        disabled={disabled}
        {...input}
      />

      {
        error && (
          <div className="error-message">
            {error}
          </div>
        )
      }
    </div>
  );
};

BaseInput.defaultProps = {
  value: undefined,
  type: 'text',
  error: '',
  input: {},
  autoComplete: 'off',
  disabled: false,
  onInput: () => {},
  onChange: () => {}
};

BaseInput.propTypes = {
  value: PropTypes.string,
  autoComplete: PropTypes.string,
  type: PropTypes.string,
  error: PropTypes.string,
  input: PropTypes.object,
  label: PropTypes.string.isRequired,
  onInput: PropTypes.func,
  onChange: PropTypes.func,
  id: PropTypes.string.isRequired,
  disabled: PropTypes.bool
};

export default BaseInput;
