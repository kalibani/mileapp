import './footer.scss';

const Footer = () => (
  <div className="footer">
    <p>© Copyright 2019 PT. Paket Informasi Digital. All Rights Reserved</p>
  </div>
);

export default Footer;
