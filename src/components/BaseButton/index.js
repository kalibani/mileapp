import React from 'react';
import PropTypes from 'prop-types';
import classname from 'classnames';
import './button.scss';

const BaseButton = ({
  onFocus, onClick, tabIndex, id, children, full, variant, disabled
}) => {
  const classNames = classname('a-button', variant, {
    'full-content': full
  });
  return (
    <>
      <button
        className={classNames}
        type="button"
        onFocus={onFocus}
        onClick={onClick}
        tabIndex={tabIndex}
        id={id}
        full={full}
        disabled={disabled}
      >
        {children}
      </button>
    </>

  );
};

BaseButton.defaultProps = {
  tabIndex: '0',
  children: '',
  id: '',
  onFocus: () => {},
  onClick: () => {},
  full: '',
  variant: '',
  disabled: false
};

BaseButton.propTypes = {
  tabIndex: PropTypes.string,
  children: PropTypes.node,
  id: PropTypes.string,
  onFocus: PropTypes.func,
  onClick: PropTypes.func,
  full: PropTypes.string,
  variant: PropTypes.string,
  disabled: PropTypes.bool
};

export default BaseButton;
