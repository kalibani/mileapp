1. Q: Do you prefer vuejs or reactjs? Why ?
   A: I choose React over Vue, because : 
      1. Popularity, the number of downloads shows the increasing popularity of React, while Vue almost doesn’t change positions.
      2. Community support. Facebook engineers are continuously working on React maintenance and coding, improving it and investing in it which makes React the most rapidly growing tool in the developers’ world.
      3. Flexibility and learning curve, React is not a full-fledged framework but a library, it’s also quite easy to learn. Instead of working in a set pattern, a developer can add any library according to their preferences. 
      4. Use Cases, React has been already used by Facebook, Twitter, Instagram, Whatsapp, Netflix etc
2.  Q: What complex things have you done in frontend development ?
    A: I have done so many things related to frontend development, the most was building a rich user interfaces, implementing the backend services, creating a middleware, help the marketing teams to create a better SEO,  implementing google analythic and facebook pixel, create a form validation and also set up the webpack for performance enhancement purposes.
3.  Q: why does a UI Developer need to know and understand UX? how far do you understand it?
    A: I think the dev need to know the ux because we need to understand the convenience of use of the application.
        I quite understand that the UI of the app don't make it difficult for the user. For some case, it is forbidden to make a dropdown of more than one step because that can make the user feel difficult to search an information.
4.  Q: Give your analysis results regarding https://taskdev.mile.app/login from the UI / UX side!
    A: I feel the UI is pretty enough and has a clear instructions for the user, but has a quite inconsistency on the mobile screen/Tablet. For example, on the ipad screen the title and logo "Mile" is align left. In the Ipad Pro the content of login form is in the top of screen. While in the desktop screen it’s on the center of the page.
5.  Q: Create a better login page based on https://taskdev.mile.app/login and deploy on https://www.netlify.com (https://www.netlify.com/)!

    A: https://mile-app-task.netlify.app/

  6.a Q Swap the values of variables A and B
      // terdapat 2 variabel A & B
      A = 3
      B = 5

      // Tukar Nilai variabel A dan B, Syarat Tidak boleh menambah Variabel Baru

      // Hasil yang diharapkan :
      A = 5
      B = 3

      A: Destructuring array ways, extract items of an array into variables.
        let a = 3;
        let b = 5;

        [a, b] = [b, a];

        console.log(a); // => 5
        console.log(b); // => 3

6.b Q: Find the missing numbers from 1 to 100
    A:
      var numbers = [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100];
      
      function findMissingNumber() {
        const found = []
        for(var i = 0; i < (numbers.length-1); i++) {
          if(numbers[i+1] - numbers[i] != 1) {
            found.push(numbers[i]+1)
          }
        }
        return found;
      }

6.c Q: return the number which is called more than 1
    A: 
    var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25, 25, 25, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 34, 34, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 83, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 92, 93, 94, 95, 96, 97, 98, 99, 100];

    function findMultipleNumber() {
      const sortered = numbers.sort((a,b) => a-b);
      const multiple = sortered.filter((el, index) => sortered.indexOf(el) !== index)
      const uniq = [...new Set(multiple)]
      return uniq;
    }

  

6.d

// soal 
let array_code = ["1.", "1.1.", "1.2.", "1.3.", "1.4.", "1.1.1.", "1.1.2.", "1.1.3.", "1.2.1.", "1.2.2.", "1.3.1.", "1.3.2.", "1.3.3.", "1.3.4.", "1.4.1.", "1.4.3."]

function generateObj (params, value) {
    let keyObj = params[0]
    let sliceparam = params.slice(1,params.length)
    if(params.length === 1 && !params[1]){
        return value
    }
    else{
        return {[keyObj]:generateObj(sliceparam,value)}
    }
}

function mergeDeepObj (obj, tmp) {
    let objKey = Object.keys(tmp)[0]
    if(Object.keys(tmp).length > 1){
        return
    } else {
        if(!obj[objKey]){
            obj[objKey] = tmp
            return {...obj, ...tmp}
        }else{
            if(typeof(tmp[objKey]) === 'string'){
                return tmp
            }
            return {...obj, [objKey]:mergeDeepObj(obj[objKey],tmp[objKey])}
        }
    }

}

function convertArraytoObj (params) {
    let result = {}
        params.forEach(el => {
            let splited = el.split('.')
            let tmp = generateObj(splited, el)
            result = mergeDeepObj(result, tmp)
        });
    return result

}
console.log(JSON.stringify(convertArraytoObj(array_code), null, 5))
